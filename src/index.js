import React, { useEffect, useState } from 'react'
import ReactDOM from 'react-dom'
import { getSentence } from './utils'
import cover from './cover.jpeg'

const onRefreshSentence = callback => () => {
  const SPACE_KEY_CODE = 32
  const onSpacePressed = ({ keyCode }) => {
    if (SPACE_KEY_CODE === keyCode) callback(getSentence())
  }

  document.addEventListener('keydown', onSpacePressed)
  return () => document.removeEventListener('keydown', onSpacePressed)
}

// eslint-disable-next-line react/prop-types
export const Badge = ({ children, withMargin = false }) => {
  const classes = [
    'inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700',
    withMargin && 'mr-2'
  ].join(' ')
  return <span className={classes}>{children}</span>
}

// eslint-disable-next-line react/prop-types
export const Text = ({ children }) => (
  <div className='px-6 py-4'>
    <p className='text-gray-700 text-base'>{children}</p>
  </div>
)

const App = () => {
  const [sentence, setSentence] = useState(getSentence())
  useEffect(onRefreshSentence(setSentence))

  return (
    <div className='max-w-sm rounded overflow-hidden shadow-lg'>
      <img className='w-full' src={cover}></img>

      <Text>{sentence}</Text>
      <div className='px-6 py-4'>
        <span className='mr-2 text-gray-500'>To reload press</span>
        <Badge withMargin={true}>Space</Badge>
        <Badge>Ctrl+R</Badge>
      </div>
    </div>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
